import pytest
from kisters.network_store.service.padp.asgi import app
from starlette.testclient import TestClient

from kisters.network_store.client.network_store import NetworkStore


@pytest.fixture
def client():
    return TestClient(app)


def test_store(client):
    store = NetworkStore(client)
    store.list_networks()
    network = store.get_network("tmp")
    network.drop()
